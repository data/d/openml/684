# OpenML dataset: rabe_166

https://www.openml.org/d/684

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

This file contains data from Regression Analysis By Example, 2nd Edition,
by Samprit Chatterjee and Bertram Price, John Wiley, 1991.
Data sets have names of the form 'rabe.xxx' where xxx is the page number
in the book where the data occurs.

For additional information, Samprit Chatterjee can be reached using
"schatter@stern.nyu.edu".

File: ../data/rabe/rabe.166

Note: there were no information about the columns in the data set,
hence automatically generated names


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/684) of an [OpenML dataset](https://www.openml.org/d/684). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/684/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/684/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/684/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

